import gym
from numpy import random

from d3qn_mask.d3qn_mask import MaskableD3QN
from sb3_contrib.ppo_mask import MaskablePPO
from gym.envs.registration import register
from typing import Callable
from stable_baselines3.common.utils import set_random_seed
from scheduling.scheduling_eval import model_evaluation, random_evaluation, json_evaluation, lst_evaluation
from scheduling.scheduling_dump import *
import json

data_path = "data/model.json"
ppo_path = "logs/maskable_ppo/SchedulingEnv-v0_1/best_model"
d3qn_path = "logs/maskable_d3qn/SchedulingEnv-v0_1/best_model"

register(
    id='SchedulingEnv-v0',
    entry_point='scheduling.scheduling_env:SchedulingEnv',
)


def make_env(env_id: str, rank: int, data_path: str, seed: int = 0, ) -> Callable:
    """
    Utility function for multiprocessed env.

    :param env_id: (str) the environment ID
    :param num_env: (int) the number of environment you wish to have in subprocesses
    :param data_path: (str) path to the json data file
    :param seed: (int) the inital seed for RNG
    :param rank: (int) index of the subprocess
    :return: (Callable)
    """

    def _init() -> gym.Env:
        env = gym.make(env_id, path=data_path)
        #env.seed(seed + rank)
        return env

    # set_random_seed(seed)
    return _init


if __name__ == '__main__':
    env = gym.make('SchedulingEnv-v0', path=data_path)
    env.analyzer.train = False

    obs = env.reset(seed=random.randint(0, 100000))
    ppo_model = MaskablePPO.load(ppo_path, env)
    print(" ")
    print("evaluate maskable PPO:")
    model_evaluation(obs, ppo_model, env)
    old_ppo = [rnb.frame.idx for rnb in env.analyzer.runnables]

    obs = env.reset(seed=random.randint(0, 100000))
    d3qn_model = MaskableD3QN.load(d3qn_path, env)
    print(" ")
    print("evaluate maskable D3QN:")
    model_evaluation(obs, d3qn_model, env)
    old_d3qn = [rnb.frame.idx for rnb in env.analyzer.runnables]

    new_ppo_ret =  {"rew":[], "idx":[]}
    old_ppo_ret =  {"rew":[], "idx":[]}
    new_d3qn_ret = {"rew":[], "idx":[]}
    old_d3qn_ret = {"rew":[], "idx":[]}
    rnd_ret =  {"rew":[], "idx":[]}

    for x in range(0, 50): 
        eval_env = gym.make('SchedulingEnv-v0', path=data_path)
        eval_env.analyzer.train = False

        for j in range(0, 100):
            random.seed(x*666+j)
            rnd_lst = random.sample(range(456), 20)
            for idx in rnd_lst:
                eval_env.analyzer.runnables[idx].length = 2 * eval_env.analyzer.runnables[idx].length 

            obs = eval_env.reset(seed=random.randint(0, 100000))
            print(" ")
            print("evaluate old ppo:")
            rew1, res1= lst_evaluation(eval_env, old_ppo)
            if res1 == True:
                old_ppo_ret["rew"].append(rew1)
                old_ppo_ret["idx"].append(j)

            
            obs = eval_env.reset(seed=random.randint(0, 100000))
            print(" ")
            print("evaluate old d3qn:")
            rew2, res2= lst_evaluation(eval_env, old_d3qn)
            if res2 == True:
                old_d3qn_ret["rew"].append(rew2)
                old_d3qn_ret["idx"].append(j)


            obs = eval_env.reset(seed=random.randint(0, 100000))
            ppo_model = MaskablePPO.load(ppo_path, eval_env)
            print(" ")
            print("evaluate new ppo:")
            rew3, res3 = model_evaluation(obs, ppo_model, eval_env)
            if res3 == True:
                new_ppo_ret["rew"].append(rew3)
                new_ppo_ret["idx"].append(j)


            obs = eval_env.reset(seed=random.randint(0, 100000))
            d3qn_model = MaskableD3QN.load(d3qn_path, eval_env)
            print(" ")
            print("evaluate new d3qn:")
            rew4, res4 = model_evaluation(obs, d3qn_model, eval_env)
            if res4 == True:
                new_d3qn_ret["rew"].append(rew4)
                new_d3qn_ret["idx"].append(j)

   
            obs = eval_env.reset(seed=random.randint(0, 100000))
            print(" ")
            print("evaluate random:")
            rew5, res5= random_evaluation(eval_env)
            if res5 == True:
                rnd_ret["rew"].append(rew5)
                rnd_ret["idx"].append(j)

            if res2 == False and res1 == False and res3 == False and res4 == False and res5 == False:
                break

    data = {"old_ppo_ret":  old_ppo_ret, 
            "old_d3qn_ret": old_d3qn_ret, 
            "new_ppo_ret":  new_ppo_ret,
            "new_d3qn_ret": new_d3qn_ret,  
            "rnd_ret":      rnd_ret
            }

    with open("logs/gen_test.json", "w+") as f:
        json.dump(data, f)
