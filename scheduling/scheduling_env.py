import gym
import numpy as np
from overrides import overrides
from gym.spaces import Discrete, Box
from scheduling.scheduling_analyzer import SchedulingAnalyzer
from scheduling.scheduling_const import *


class SchedulingEnv(gym.Env):
    def __init__(self, path: str = "data/model.json"):
        super(SchedulingEnv, self).__init__()
        self.analyzer = SchedulingAnalyzer()
        self.analyzer.load_data(path)
        self.action_space = Discrete(len(self.analyzer.frames))

        # self.observation_space = Box(-1.0,
        #                              len(self.analyzer.frames),
        #                              shape=[len(self.analyzer.runnables), ], dtype=float)

        self.observation_space = Box(-1.0 / OBS_SCALE_COF - OBS_OFFSET_COF,
                                     len(self.analyzer.frames) / OBS_SCALE_COF - OBS_OFFSET_COF,
                                     shape=[len(self.analyzer.runnables), ], dtype=float)

        # self.cur_obs = np.full(shape=[len(self.analyzer.runnables), ],
        #                        fill_value=-1.0)

        self.cur_obs = np.full(shape=[len(self.analyzer.runnables), ],
                               fill_value=-1.0 / OBS_SCALE_COF - OBS_OFFSET_COF)

        self.action_mask = np.full(shape=[len(self.analyzer.frames), ], fill_value=False, dtype=bool)

        self.step_cnt = 0
        self.total_steps = len(self.analyzer.runnables)

        self.total_rewards = 0
        self.delay_rewards = 0
        self.cpu_rewards = 0
        self.mem_rewards = 0
        self.contention_rewards = 0

        self.done = False

    def render(self, mode="human"):
        pass

    def reset(self, return_info=False, seed=None, options=None):
        super().reset(seed=seed, return_info=return_info, options=options)
        self.analyzer.reset()
        #
        # self.cur_obs = np.full(shape=[len(self.analyzer.runnables), ],
        #                        fill_value=-1.0)

        self.cur_obs = np.full(shape=[len(self.analyzer.runnables), ],
                               fill_value=-1.0 / OBS_SCALE_COF - OBS_OFFSET_COF)

        self.step_cnt = 0

        self.total_rewards = 0
        self.delay_rewards = 0
        self.cpu_rewards = 0
        self.mem_rewards = 0
        self.contention_rewards = 0

        return self.cur_obs

    @overrides
    def step(self, action):

        rnb = self.analyzer.runnables[self.step_cnt]
        # at i-th step, the i-th runnable is allocated to frame[action].

        frame = self.analyzer.frames[action]
        rewards, infos = self.analyzer.update(rnb, frame)

        if self.action_mask[frame.idx] is False:
            print("runnable " + rnb + " has illegal action")
        # self.cur_obs[rnb.idx] = frame.idx
        self.cur_obs[rnb.idx] = float(frame.idx) / OBS_SCALE_COF - OBS_OFFSET_COF

        done = True if self.step_cnt == (len(self.analyzer.runnables)-1) else False

        self.step_cnt += 1

        if done:
            pass

        # info = " rnb " + str(rnb.idx) + " to frame " + str(frame.idx)
        # print(info)
        return self.cur_obs, rewards['total'], done, infos

    def action_masks(self):
        self.action_mask = np.full(shape=[len(self.analyzer.frames), ], fill_value=False, dtype=bool)
        # self.action_mask[self.action_mask == True] = False
        # nxt_tuple = np.where(self.cur_obs == -1)
        # if len(nxt_tuple) > 0 and len(nxt_tuple[0]) > 0:
        #     nxt_rnb = nxt_tuple[0][0]
        if self.step_cnt < len(self.analyzer.runnables):
            nxt_rnb = self.analyzer.runnables[self.step_cnt]
            self.analyzer.calc_action_mask(nxt_rnb, self.action_mask)

        return self.action_mask
