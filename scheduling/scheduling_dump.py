import os

from scheduling.scheduling_analyzer import *


def scheduling_dump(a: SchedulingAnalyzer, prefix="", path=""):
    frame_lst = []
    signal_lst = []

    for frame in a.frames:
        read_sig_names = set()
        write_sig_names = set()
        for rnb in frame.rnbs:
            for sig in rnb.read_signals:
                str_name = "Signal_" + str(sig.idx)
                if str_name not in read_sig_names:
                    read_sig_names.add(str_name)

            for sig in rnb.write_signals:
                str_name = "Signal_" + str(sig.idx)
                if str_name not in write_sig_names:
                    write_sig_names.add(str_name)
        join_set = read_sig_names & write_sig_names

        read_sig_names = read_sig_names - join_set
        write_sig_names = write_sig_names - join_set

        frame_dict = {
            "Name": "Frame_" + str(frame.idx),
            "Index": frame.idx,
            "CoreId": str(frame.core_idx),
            "Type": "LET",
            "OffsetMs": float(frame.offset)/1000,
            "PeriodMs": float(frame.period)/1000,
            "LengthMs": float(frame.length_alloc)/1000,
            "AllInputs": list(read_sig_names),
            "AllOutputs": list(write_sig_names)
        }
        frame_lst.append(frame_dict)

    for sig in a.signals:
        reader_frame_names = set()
        writer_frame_names = set()
        for reader_rnb in sig.readers:
            frame_name = "Frame_" + str(reader_rnb.frame.idx)
            if frame_name not in reader_frame_names:
                reader_frame_names.add(frame_name)

        for writer_rnb in sig.readers:
            frame_name = "Frame_" + str(writer_rnb.frame.idx)
            if frame_name not in writer_frame_names:
                writer_frame_names.add(frame_name)

        sig_dic = {"Readers": list(reader_frame_names),
                   "Writers": list(writer_frame_names),
                   "Name": "Signal_" + str(sig.idx),
                   "Dim": 1,
                   "Size": sig.data_vol,
                   "Index": sig.idx}
        signal_lst.append(sig_dic)

    path = os.path.join(path, prefix)

    frame_path = os.path.join(path, "Frames.json")
    signal_path = os.path.join(path, "Signals.json")

    if not os.path.isdir(path):
        os.mkdir(path)

    with open(frame_path, "w+") as f:
        json.dump(frame_lst, f)

    with open(signal_path, "w+") as f:
        json.dump(signal_lst, f)
