from scheduling.scheduling_const import *


class Signal(object):
    def __init__(self, idx: int, data_vol: int):
        self.idx = idx
        self.data_vol = data_vol
        self.readers = []
        self.writers = []
        self.reader_idxs = []
        self.writer_idxs = []
        self.dist_mean = {}
        # self.dist_std = {}

    def append_access(self, rnb, t):
        if t == 'r':
            self.readers.append(rnb)
            self.reader_idxs.append(rnb.idx)
        else:
            self.writers.append(rnb)
            self.writer_idxs.append(rnb.idx)

    def get_access(self, t):
        if t == 'r':
            return self.readers
        elif t == 'w':
            return self.writers


class Runnable(object):
    def __init__(self, idx: int, period: int, length: int):
        self.idx = idx
        self.period = period
        self.length = length
        self.read_signals = []
        self.read_signal_idxs = []
        self.write_signals = []
        self.write_signal_idxs = []
        self.read_data_vol = 0
        self.write_data_vol = 0

        self.avbl_frames = []
        self.avbl_frame_idxs = []
        self.frame = None

    def append_signal(self, s: Signal, t):
        if t == 'r':
            self.read_signals.append(s)
            self.read_signal_idxs.append(s.idx)
            self.read_data_vol += s.data_vol
        elif t == 'w':
            self.write_signals.append(s)
            self.write_signal_idxs.append(s.idx)
            self.write_data_vol += s.data_vol

    def append_avbl_frame(self, frame):
        self.avbl_frames.append(frame)
        self.avbl_frame_idxs.append(frame.idx)

    def alloc_to(self, frame):
        self.frame = frame

    def reset(self):
        self.frame = None


class TaskFrame(object):
    def __init__(self, idx: int, period: int, offset: int, length_alloc: int, core_idx: int):
        self.idx = idx
        self.period = period
        self.offset = offset
        self.length_alloc = length_alloc
        self.core_idx = core_idx
        self.read_contentions = []
        self.write_contentions = []
        self.read_contention_idxs = []
        self.write_contention_idxs = []

        self.rnbs = []
        self.length_used = 0
        self.read_signal_idxs = set()
        self.write_signal_idxs = set()
        self.read_data_vol = 0
        self.write_data_vol = 0

    def is_full(self, rnb_length):
        if self.length_used + rnb_length >= (self.length_alloc * FRAME_FULL_COF):
            return True
        else:
            return False
            
    def add_rnb(self, r: Runnable):
        self.rnbs.append(r)
        self.length_used += r.length

        for sig in r.read_signals:
            if sig.idx not in self.read_signal_idxs:
                self.read_data_vol += sig.data_vol
                self.read_signal_idxs.add(sig.idx)
        
        for sig in r.write_signals:
            if sig.idx not in self.write_signal_idxs:
                self.write_data_vol += sig.data_vol
                self.write_signal_idxs.add(sig.idx)

    def add_contention(self, ac, access_type):
        if access_type == 'r':
            self.read_contentions.append(ac)
            self.read_contention_idxs.append(ac.idx)
        else:
            self.write_contentions.append(ac)
            self.write_contention_idxs.append(ac.idx)

    def reset(self):
        list(map(lambda x: x.reset(), self.read_contentions))
        list(map(lambda x: x.reset(), self.write_contentions))
        self.rnbs = []
        self.length_used = 0
        self.read_signal_idxs = set()
        self.write_signal_idxs = set()
        self.read_data_vol = 0
        self.write_data_vol = 0


class AccessInstance(object):
    def __init__(self, frame: TaskFrame, task_idx: int, access_idx: int, access_type: str):
        self.frame = frame
        self.access_type = access_type
        self.task_idx = task_idx
        self.idx = access_idx
        inst_start = frame.offset + frame.period * task_idx
        inst_end = inst_start + frame.length_alloc

        if access_type == 'r':
            self.start = inst_start + READ_OFFSET
            self.end = self.start + READ_LEN

        elif access_type == 'w':
            self.end = inst_end - WRITE_OFFSET
            self.start = inst_end - WRITE_LEN


class AccessContentions(object):
    def __init__(self, idx):
        self.sig_volume = 0
        self.idx = idx

    def reset(self):
        self.sig_volume = 0

