import numpy as np
import json
from scheduling.scheduling_const import *


def model_evaluation(obs, model, env):
    eps_reward = 0

    for i in range(env.total_steps):
        action_mask = env.action_masks()
        action, _ = model.predict(observation=obs, action_masks=action_mask, deterministic=True)
        obs, reward, done, info = env.step(action)
        eps_reward += reward

    sum_delay, _, _ = eval_sig_delay(env.analyzer)
    cpu_load_std = eval_cpu_load(env.analyzer)
    mem_load_std = eval_mem_load(env.analyzer)
    res = eval_frame_len(env.analyzer)
    vols = eval_contention(env.analyzer)

    print(f'reward in evaluation: {eps_reward}')
    print(f'sum_delays in evaluation: {sum_delay}')
    print(f'cpu_load_std in evaluation: {cpu_load_std}')
    print(f'mem_load_std in evaluation: {mem_load_std}')
    print(f'signal_volume under contention in evaluation: {vols}')
    print(f' ')

    return eps_reward, res


def random_evaluation(env):
    eps_reward = 0
    for i in range(env.total_steps):
        action_mask = env.action_masks()
        action_tuple = np.where(action_mask == True)
        if len(action_tuple) == 0 or len(action_tuple[0]) == 0:
            return 0, False
        random_int = np.random.randint(len(action_tuple[0]), size=1)
        action = action_tuple[0][random_int[0]]
        obs, reward, done, info = env.step(action)
        eps_reward += reward

    sum_delay, _, _ = eval_sig_delay(env.analyzer)
    cpu_load_std = eval_cpu_load(env.analyzer)
    mem_load_std = eval_mem_load(env.analyzer)
    res = eval_frame_len(env.analyzer)
    vols = eval_contention(env.analyzer)

    print(f'reward in evaluation: {eps_reward}')
    print(f'sum_delays in evaluation: {sum_delay}')
    print(f'cpu_load_std in evaluation: {cpu_load_std}')
    print(f'mem_load_std in evaluation: {mem_load_std}')
    print(f'signal_volume under contention in evaluation: {vols}')
    print(f' ')
    return eps_reward, res


def json_evaluation(env, path):
    eps_reward = 0
    json_file = open(path)
    dic = json.load(json_file)
    lst = dic['result']
    rew, res = lst_evaluation(env, lst)
    return rew, res


def lst_evaluation(env, lst):
    eps_reward = 0
    for i in range(env.total_steps):
        if i == 309:
            pass
        action_mask = env.action_masks()
        action = lst[i]

        if not action_mask[action]:
            pstr = "Runnable " + str(i) + " to Frame " + str(action) + " is forbidden in mask"
            print(pstr)
            rnb_len = env.analyzer.runnables[i].length
            rnb_id = env.analyzer.runnables[i].idx
            frame_id = env.analyzer.frames[action].idx
            frame_free = env.analyzer.frames[action].length_alloc * FRAME_FULL_COF - env.analyzer.frames[action].length_used
            print("Runnable " + str(rnb_id) + " length: " + str(rnb_len) + " , free Frame " +str(frame_id) + " length: " + str(frame_free))
            return 0, False
        obs, reward, done, info = env.step(action)
        eps_reward += reward

    sum_delay, _, _ = eval_sig_delay(env.analyzer)
    cpu_load_std = eval_cpu_load(env.analyzer)
    mem_load_std = eval_mem_load(env.analyzer)
    res = eval_frame_len(env.analyzer)
    vols = eval_contention(env.analyzer)

    print(f'reward in evaluation: {eps_reward}')
    print(f'sum_delay in evaluation: {sum_delay}')
    print(f'cpu_load_std in evaluation: {cpu_load_std}')
    print(f'mem_load_std in evaluation: {mem_load_std}')
    print(f'signal_volume under contention in evaluation: {vols}')
    print(f' ')
    return eps_reward, res

def eval_sig_delay(analyzer):
    sig_delay = []
    for frame in analyzer.frames:
        for rnb in frame.rnbs:
            for sig in rnb.read_signals:
                for writer in sig.get_access('w'):
                    if writer.frame is not None:
                        sig_delay.append(analyzer.frame_dists[writer.frame.idx, frame.idx])

            for sig in rnb.write_signals:
                for reader in sig.get_access('r'):
                    if reader.frame is not None:
                        sig_delay.append(analyzer.frame_dists[frame.idx, reader.frame.idx])

    acc_sig_delay = np.sum(sig_delay)/2
    mean_sig_delay = np.mean(sig_delay)
    std_sig_delay = np.std(sig_delay)
    return acc_sig_delay, mean_sig_delay, std_sig_delay


def eval_frame_len(analyzer):
    res = True
    for frame in analyzer.frames:
        len_used = 0
        for rnb in frame.rnbs:
            len_used += rnb.length
        assert len_used == frame.length_used

        if len_used > frame.length_alloc * FRAME_FULL_COF:
            res = False
            print("frame " + str(frame.idx) + " exceed length safety upper bound: " + str(len_used/frame.length_alloc))
    return res


def eval_cpu_load(analyzer):
    val = np.sum(analyzer.cpu_loads)
    # return analyzer.cpu_loads / val
    return np.std(analyzer.cpu_loads)


def eval_mem_load(analyzer):
    val = np.sum(analyzer.mem_loads)
    return np.std(analyzer.mem_loads)


def eval_contention(analyzer):
    vols = [item.sig_volume for item in analyzer.contentions]
    return np.sum(vols)