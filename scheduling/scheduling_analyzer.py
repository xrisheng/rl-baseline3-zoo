import copy
import random
import numpy as np
import math
import json
import pickle

from scheduling.scheduling_data import TaskFrame, AccessInstance, AccessContentions, Runnable, Signal
from scheduling.scheduling_const import *


class SchedulingAnalyzer(object):
    def __init__(self):
        self.frames = []
        self.runnables = []
        self.signals = []
        self.frame_dists = np.array(0)
        self.contentions = []
        self.cpu_loads = [0, 0, 0, 0]
        self.mem_loads = [0, 0, 0, 0]
        self.frame_read_sig_vol = []
        self.frame_write_sig_vol = []
        self.acc_rew_total = 0.0
        self.acc_rew_sig_delay = 0.0
        self.acc_rew_cpu_load = 0.0
        self.acc_rew_mem_load = 0.0
        self.acc_rew_contention = 0.0
        self.episode = 0
        self.rescale = False
    def __deepcopy__(self, memodict={}):
        s = self.copy_const()
        s.cpu_loads = pickle.loads(pickle.dumps(self.cpu_loads))
        s.mem_loads = pickle.loads(pickle.dumps(self.mem_loads))

        s.frame_read_sig_vol = pickle.loads(pickle.dumps(self.frame_read_sig_vol))
        s.frame_write_sig_vol = pickle.loads(pickle.dumps(self.frame_write_sig_vol))

        s.acc_rew_total = self.acc_rew_total
        s.acc_rew_sig_delay = self.acc_rew_sig_delay
        s.acc_rew_cpu_load = self.acc_rew_cpu_load
        s.acc_rew_mem_load = self.acc_rew_mem_load
        s.acc_rew_contention = self.acc_rew_contention

        for rnb_idx in range(len(self.runnables)):
            frame_idx = self.runnables[rnb_idx].frame.idx
            frame = s.frames[frame_idx]
            rnb = s.runnables[rnb_idx]

            frame.add_rnb(rnb)
            rnb.alloc_to(frame)

        return s

    def reset(self):
        list(map(lambda x: x.reset(), self.frames))
        list(map(lambda x: x.reset(), self.runnables))
        self.cpu_loads = [0, 0, 0, 0]
        self.mem_loads = [0, 0, 0, 0]

        self.frame_read_sig_vol = [0]* len(self.frames)
        self.frame_write_sig_vol = [0]* len(self.frames)

        self.acc_rew_total = 0.0
        self.acc_rew_sig_delay = 0.0
        self.acc_rew_cpu_load = 0.0
        self.acc_rew_mem_load = 0.0
        self.acc_rew_contention = 0.0

        if self.rescale:
            self.episode += 1

    def clear_episode_cnt(self):
        self.episode = 0

    def update(self, rnb: Runnable, frame: TaskFrame):
        frame.add_rnb(rnb)
        rnb.alloc_to(frame)

        rewards = self.__get_reward(rnb, frame)

        self.acc_rew_total += rewards['total']
        self.acc_rew_sig_delay += rewards['rew_sig_delay']
        self.acc_rew_cpu_load += rewards['rew_cpu_load']
        self.acc_rew_mem_load += rewards['rew_mem_load']
        self.acc_rew_contention += rewards['rew_contention']

        infos = {'rew_total': self.acc_rew_total,
                 'rew_sig_delay': self.acc_rew_sig_delay,
                 'rew_cpu_load': self.acc_rew_cpu_load,
                 'rew_mem_load': self.acc_rew_mem_load,
                 'rew_contention': self.acc_rew_contention}

        return rewards, infos

    def rnd_scheduling(self):
        for rnb in self.runnables:
            frame_idxs = [frame.idx for frame in rnb.avbl_frames]
            random.shuffle(frame_idxs)
            for idx in frame_idxs:
                if not self.frames[idx].is_full(rnb.length):
                    _, _ = self.update(rnb, self.frames[idx])
                    break
            if rnb.frame is None:
                print("not scheduable runnable " + rnb.idx)
        pass

    def copy_const(self):
        s = SchedulingAnalyzer()
        s.signals = [Signal(sig.idx, sig.data_vol) for sig in self.signals]
        s.runnables = [Runnable(rnb.idx, rnb.period, rnb.length) for rnb in self.runnables]
        s.frames = [TaskFrame(frame.idx, frame.period, frame.offset, frame.length_alloc, frame.core_idx)
                    for frame in self.frames]
        s.contentions = [AccessContentions(idx) for idx in range(len(self.contentions))]

        for idx in range(len(self.signals)):
            s.signals[idx].readers = [s.runnables[rnb_idx] for rnb_idx in self.signals[idx].reader_idxs]
            s.signals[idx].writers = [s.runnables[rnb_idx] for rnb_idx in self.signals[idx].writer_idxs]
            s.signals[idx].reader_idxs = pickle.loads(pickle.dumps(self.signals[idx].reader_idxs))
            s.signals[idx].writer_idxs = pickle.loads(pickle.dumps(self.signals[idx].writer_idxs))
            s.signals[idx].dist_mean = pickle.loads(pickle.dumps(self.signals[idx].dist_mean))

        for idx in range(len(self.runnables)):
            s.runnables[idx].read_data_vol = self.runnables[idx].read_data_vol
            s.runnables[idx].write_data_vol = self.runnables[idx].write_data_vol
            s.runnables[idx].read_signals = [s.signals[sig_idx] for sig_idx in self.runnables[idx].read_signal_idxs]
            s.runnables[idx].write_signals = [s.signals[sig_idx] for sig_idx in self.runnables[idx].write_signal_idxs]
            s.runnables[idx].avbl_frames = [s.frames[frame_idx] for frame_idx in self.runnables[idx].avbl_frame_idxs]
            s.runnables[idx].read_signal_idxs = pickle.loads(pickle.dumps(self.runnables[idx].read_signal_idxs))
            s.runnables[idx].write_signal_idxs = pickle.loads(pickle.dumps(self.runnables[idx].write_signal_idxs))
            s.runnables[idx].avbl_frame_idxs = pickle.loads(pickle.dumps(self.runnables[idx].avbl_frame_idxs))

        for idx in range(len(self.frames)):
            s.frames[idx].read_contentions = [s.contentions[cont_idx] for cont_idx in self.frames[idx].read_contention_idxs]
            s.frames[idx].write_contentions = [s.contentions[cont_idx] for cont_idx in self.frames[idx].write_contention_idxs]
            s.frames[idx].read_contention_idxs = pickle.loads(pickle.dumps(self.frames[idx].read_contention_idxs))
            s.frames[idx].write_contention_idxs = pickle.loads(pickle.dumps(self.frames[idx].write_contention_idxs))
        s.frame_dists = np.copy(self.frame_dists)

        return s

    def calc_action_mask(self, rnb: Runnable, mask: np.ndarray):
        # First, select frames with same period
        avbl_idx = [frame.idx for frame in rnb.avbl_frames]

        # Second, check if frames is full occupied
        for idx in avbl_idx:
            if not self.frames[idx].is_full(rnb.length):
                mask[idx] = True

        return mask

    def load_data(self, data_path):
        self.frames.clear()
        self.runnables.clear()
        self.signals.clear()

        json_file = open(data_path)
        data = json.load(json_file)

        for frame in data["Frames"]:
            self.frames.append(TaskFrame(frame["Index"], frame["Period"], frame["Offset"], frame["LengthAlloc"], frame["CoreID"]))

        for rnb in data["Runnables"]:
            self.runnables.append(Runnable(rnb["Index"], rnb["Period"], rnb["Length"]))

        for signal in data["Signals"]:
            self.signals.append(Signal(signal["Index"], signal["Size"]))

        self.frame_dists = np.zeros((len(self.frames), len(self.frames)))

        for signal in data["Signals"]:
            for reader_idx in signal["Readers"]:
                self.signals[signal["Index"]].append_access(self.runnables[reader_idx], 'r')
            for writer_idx in signal["Writers"]:
                self.signals[signal["Index"]].append_access(self.runnables[writer_idx], 'w')

        for rnb in data["Runnables"]:
            for read_sig_idx in rnb["ReadSignals"]:
                self.runnables[rnb["Index"]].append_signal(self.signals[read_sig_idx], 'r')

            for write_sig_idx in rnb["WriteSignals"]:
                self.runnables[rnb["Index"]].append_signal(self.signals[write_sig_idx], 'w')

        for rnb in self.runnables:
            for frame in self.frames:
                if rnb.period == frame.period:
                    rnb.append_avbl_frame(frame)
        
        self.frame_read_sig_vol = [0] * len(self.frames)
        self.frame_write_sig_vol = [0] * len(self.frames)

        self. __init_frame_dist()
        self.__init_contention_estimation()
        self.__stk_sig_delay()

    def __opt_sig_delay(self, rnb: Runnable, frame: TaskFrame):
        res = 0
        for sig in rnb.read_signals:
            for writer in sig.get_access('w'):
                if writer.frame is not None:
                    mean_dist = sig.dist_mean[writer.idx][rnb.idx]
                    # std_dist = sig.dist_std[writer.idx][rnb.idx]+1
                    res += (mean_dist - self.frame_dists[writer.frame.idx, frame.idx]) / SIG_DELAY_COF

        for sig in rnb.write_signals:
            for reader in sig.get_access('r'):
                if reader.frame is not None:
                    mean_dist = sig.dist_mean[rnb.idx][reader.idx]
                    # std_dist = sig.dist_std[rnb.idx][reader.idx]+1
                    res += (mean_dist - self.frame_dists[frame.idx, reader.frame.idx]) / SIG_DELAY_COF

        return res

    def __opt_cpu_load(self, rnb: Runnable, frame: TaskFrame):
        idx = 0
        if frame.core_idx == "A0":
            idx = 0
        elif frame.core_idx == "A1":
            idx = 1
        elif frame.core_idx == "A2":
            idx = 2
        elif frame.core_idx == "A3":
            idx = 3
        else:
            print("Unrecognized Core in CPU Load")

        old_std = np.std(self.cpu_loads)
        self.cpu_loads[idx] += rnb.length
        new_std = np.std(self.cpu_loads)

        res = (old_std - new_std) / CPU_LOAD_COF

        # if self.rescale:
        #     res = res * self.episode / EPISODE_COF

        return res

    def __opt_mem_load(self, rnb: Runnable, frame: TaskFrame):
        idx = 0
        if frame.core_idx == "A0":
            idx = 0
        elif frame.core_idx == "A1":
            idx = 1
        elif frame.core_idx == "A2":
            idx = 2
        elif frame.core_idx == "A3":
            idx = 3
        else:
            print("Unrecognized Core in Memory Load")

        old_std = np.std(self.mem_loads)
        for sig in rnb.read_signals:
            self.mem_loads[idx] += sig.data_vol
        for sig in rnb.write_signals:
            self.mem_loads[idx] += sig.data_vol
        new_std = np.std(self.mem_loads)
        res = (old_std - new_std) / MEM_LOAD_COF

        # if self.rescale:
        #     res = res * self.episode / EPISODE_COF 

        return res

    def __opt_contention(self, rnb, frame):
        idx = frame.idx
        res = 0

        old_read_vol = self.frame_read_sig_vol[idx]
        new_read_vol = frame.read_data_vol

        old_write_vol = self.frame_write_sig_vol[idx]
        new_write_vol = frame.write_data_vol

        self.frame_read_sig_vol[idx] = new_read_vol
        self.frame_write_sig_vol[idx] = new_write_vol

        delta_read_vol = new_read_vol - old_read_vol
        delta_write_vol = new_write_vol - old_write_vol

        # res = len(frame.read_contentions) * rnb.read_data_vol + \
        #       len(frame.write_contentions) * rnb.write_data_vol
        
        # res = len(frame.read_contentions) * delta_read_vol + \
        #       len(frame.write_contentions) * delta_write_vol

        if len(frame.read_contentions) > 0:
            for item in frame.read_contentions:
                item.sig_volume += delta_read_vol
                if item.sig_volume > MAX_CONTENTION:
                    res = 100000000

        if len(frame.write_contentions) > 0:
            for item in frame.write_contentions:
                item.sig_volume += delta_write_vol
                if item.sig_volume > MAX_CONTENTION:
                    res = 100000000

        res = -res / CONTENTION_COF 

        # if self.rescale:
        #     res = res * self.episode / EPISODE_COF 

        return res

    def __get_reward(self, rnb, frame):

        rew_sig_delay = self.__opt_sig_delay(rnb, frame)
        rew_cpu_load = self.__opt_cpu_load(rnb, frame)
        rew_mem_load = self.__opt_mem_load(rnb, frame)
        rew_contention = self.__opt_contention(rnb, frame)
        total = rew_sig_delay + rew_cpu_load + rew_mem_load + rew_contention

        # print(f'rew_sig_delay: {rew_sig_delay}')
        # print(f'rew_cpu_load: {rew_cpu_load}')
        # print(f'rew_mem_load: {rew_mem_load}')
        # print(f'rew_contention: {rew_contention}')

        res = {'total': total, 'rew_sig_delay': rew_sig_delay,
               'rew_cpu_load': rew_cpu_load, 'rew_mem_load': rew_mem_load, 'rew_contention': rew_contention}
        return res

    def __stk_sig_delay(self):

        for sig in self.signals:
            for reader in sig.get_access('r'):
                for writer in sig.get_access('w'):
                    avbl_dist = np.zeros((len(reader.avbl_frames), len(writer.avbl_frames)))
                    for reader_frame_cnt in range(len(reader.avbl_frames)):
                        for writer_frame_cnt in range(len(writer.avbl_frames)):
                            writer_frame = writer.avbl_frames[writer_frame_cnt]
                            reader_frame = reader.avbl_frames[reader_frame_cnt]
                            avbl_dist[reader_frame_cnt, writer_frame_cnt] = self.frame_dists[writer_frame.idx, reader_frame.idx]

                    if writer.idx not in sig.dist_mean:
                        sig.dist_mean[writer.idx] = {}
                    sig.dist_mean[writer.idx][reader.idx] = np.mean(avbl_dist)

                    # if writer.idx not in sig.dist_std:
                    #     sig.dist_std[writer.idx] = {}
                    # sig.dist_std[writer.idx][reader.idx] = np.std(avbl_dist)

        pass

    def __init_frame_dist(self):
        for left in self.frames:
            for right in self.frames:
                if left.idx == right.idx:
                    self.frame_dists[left.idx, right.idx] = 0
                else:
                    hyper_period = math.lcm(left.period, right.period)
                    right_num = math.floor((hyper_period - right.offset - 1) / right.period) + 1
                    right_start = [x * right.period + right.offset for x in range(right_num)]
                    max_diff = 0

                    for val in right_start:
                        last_left_idx = math.floor((val - left.offset - left.length_alloc - 1) / left.period)
                        last_left_end = last_left_idx * left.period + left.offset + left.length_alloc
                        diff = val - last_left_end
                        max_diff = diff if diff > max_diff else max_diff
                    self.frame_dists[left.idx, right.idx] = max_diff
        pass

    def __init_contention_estimation(self):
        access_insts = self.__gen_access_inst(GEN_ACCESS_INTERVAL)
        access_patterns = self.__find_access_pattern(access_insts)
        self.__find_contentions(access_patterns)

    def __gen_access_inst(self, mus):
        cnt = 0
        access_insts = []
        for frame in self.frames:
            inst_num = int(((mus - frame.offset - 1) / frame.period) + 1)

            for i in range(inst_num):
                access_insts.append(AccessInstance(frame, i, cnt, 'r'))
                cnt += 1
                access_insts.append(AccessInstance(frame, i, cnt, 'w'))
                cnt += 1

        return access_insts

    @staticmethod
    def __find_access_pattern(access_insts):
        res = {}

        for l_inst in access_insts:
            for r_inst in access_insts:

                if l_inst.frame.core_idx == r_inst.frame.core_idx:
                    continue

                # -------------------------------------------------------------
                #                       | ---------- |
                #                       | l_instance |
                #                       | ---------- |
                # | ---------- |
                # | r_instance |
                # | ---------- |
                # ------------------------------------------------------------> TIME
                if r_inst.end <= l_inst.start:
                    continue

                # -------------------------------------------------------------
                # | ---------- |
                # | l_instance |
                # | ---------- |
                #                  | ---------- |
                #                  | r_instance |
                #                  | ---------- |
                # ------------------------------------------------------------> TIME
                elif l_inst.end <= r_inst.start:
                    continue

                # -------------------------------------------------------------
                #         | ---------- |
                #         | l_instance |
                #         | ---------- |
                # | ---------- |
                # | r_instance |
                # | ---------- |
                # ------------------------------------------------------------> TIME
                else:
                    if l_inst.idx not in res:
                        res[l_inst.idx] = [l_inst]
                    res[l_inst.idx].append(r_inst)

        return res

    def __find_contentions(self, access_patterns):

        exists_pattern = set()

        for _, value in access_patterns.items():
            if len(value) >= MAX_OVERLAP:
                exist_lst = [inst.frame.idx * 1000 if inst.access_type == 'r' else inst.frame.idx * 1000 + 1
                             for inst in value]
                exist_lst.sort()
                exist_key = tuple(exist_lst)
                if exist_key not in exists_pattern:
                    exists_pattern.add(exist_key)
                    ac = AccessContentions(len(self.contentions))
                    self.contentions.append(ac)
                    for item in value:
                        item.frame.add_contention(ac, item.access_type)
        pass


if __name__ == '__main__':

    import cProfile
    a = SchedulingAnalyzer()
    a.load_data("data/mb.json")
    a.rnd_scheduling()
    pr = cProfile.Profile()
    pr.enable()
    c = copy.deepcopy(a)
    pr.disable()
    pr.print_stats(sort='cumtime')
    pass

