# Reduing Overall Path Latency in Automotive LET Scheduling via Reinforcement Learning
This is the source code for paper "Automotive LET Scheduling via Reinforcement Learning". The code is a branch of the [RL Baseline3 Zoo](https://github.com/DLR-RM/rl-baselines3-zoo). please check the original repository for detailed tutorial

## Usage
- Train maskable PPO algorithm (check `./hyperparams/maskable_ppo.yml` for hyperparamters)
```
python train.py --algo, maskable_ppo, --env, SchedulingEnv-v0, --tensorboard-log, logs/tb/maskable_ppo, --eval-freq, 3000
```
- Train maskable D3QN algorithm (check `./hyperparams/maskable_d3qn.yml` for hyperparamters)
```
python train.py --algo, maskable_d3qn, --env, SchedulingEnv-v0, --tensorboard-log, logs/tb/maskable_d3qn, --eval-freq, 3000
```
- Train genetic algorithm algorithm
```
python train_ga.py
```
- Evaluate different models
```
python evaluate_models.py
```
- Evaluate generailization ability
```
python evaluate_generalization.py
```

## Important Contents
### ./data
The `model.json` containts the models of target software, describes the properties of different signals, runnables and frames. The `model2.json` is the dummy model indicates the software after a major change. The `baseline.json` is the current scheduling.

### ./logs
This folder contains the result of all experiments. The `ga` is the result from genetic algorithm, the `maskable_d3qn` is the trained agent from D3QN algorithm, the `maskable_ppo` is the agent from PPO algorithm. the `tb` folder contains the training log in tensor board format. Both RL algorithms contains three agent. The first is the best agent derived from `model.json`, the second is the reproduced best agent, the third model is based on `model2.json`. The `gen_test.json` is the result from generalization test.

### ./scheduling
It contains the code related to RL environments.

### ./d3qn_mask
This is the implementation of maskable D3QN algorithm based on [Stable-Baseline 3 sb3-corelib](https://github.com/Stable-Baselines-Team/stable-baselines3-contrib/tree/master/sb3_contrib)