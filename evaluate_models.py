import gym
from numpy import random

from d3qn_mask.d3qn_mask import MaskableD3QN
from sb3_contrib.ppo_mask import MaskablePPO
from gym.envs.registration import register
from typing import Callable
from stable_baselines3.common.utils import set_random_seed
from scheduling.scheduling_eval import model_evaluation, random_evaluation, json_evaluation
from scheduling.scheduling_dump import *

data_path = "data/model.json"
baseline_path = "data/baseline.json"

ppo_path = "logs/maskable_ppo/SchedulingEnv-v0_1/best_model"
d3qn_path = "logs/maskable_d3qn/SchedulingEnv-v0_1/best_model"
ga_path = "logs/ga/model_model_20221021-162909.json"

register(
    id='SchedulingEnv-v0',
    entry_point='scheduling.scheduling_env:SchedulingEnv',
)


def make_env(env_id: str, rank: int, data_path: str, seed: int = 0, ) -> Callable:
    """
    Utility function for multiprocessed env.

    :param env_id: (str) the environment ID
    :param num_env: (int) the number of environment you wish to have in subprocesses
    :param data_path: (str) path to the json data file
    :param seed: (int) the inital seed for RNG
    :param rank: (int) index of the subprocess
    :return: (Callable)
    """

    def _init() -> gym.Env:
        env = gym.make(env_id, path=data_path)
        env.seed(seed + rank)
        return env

    set_random_seed(seed)
    return _init


if __name__ == '__main__':
    # Note that use of masks is manual and optional outside of learning,
    # so masking can be "removed" at testing time
    eval_env = gym.make('SchedulingEnv-v0', path=data_path)
    obs = eval_env.reset(seed=random.randint(0, 1000000))
    eval_env.analyzer.train = False

    d3qn_model = MaskableD3QN.load(d3qn_path, eval_env)
    print(" ")
    print("evaluate maskable D3QN:")
    model_evaluation(obs, d3qn_model, eval_env)

    obs = eval_env.reset(seed=random.randint(0, 100000))
    ppo_model = MaskablePPO.load(ppo_path,eval_env)
    print(" ")
    print("evaluate maskable PPO:")
    model_evaluation(obs, ppo_model, eval_env)

    print(" ")
    print("evaluate genetic algorithm:")
    obs = eval_env.reset(seed=random.randint(0, 1000000))
    json_evaluation(eval_env, ga_path)

    print(" ")
    print("evaluate baseline policy:")
    obs = eval_env.reset(seed=random.randint(0, 1000000))
    json_evaluation(eval_env, baseline_path)

    print(" ")
    print("evaluate random policy:")
    obs = eval_env.reset(seed=random.randint(0, 1000000))
    random_evaluation(eval_env)





