import json
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

import numpy as np
import pandas as pd
import csv

with open("logs/gen_test.json") as f:
    d = json.load(f)

data1 = d["new_ppo_ret"]
data2 = d["new_d3qn_ret"]
data3 = d["rnd_ret"]

# plt.scatter(data1["idx"], data1["rew"], c ="red", s=1, label= "PPO")
plt.scatter(data2["idx"], data2["rew"], c ="blue", s=1, label= "D3QN")
# plt.scatter(data3["idx"], data3["rew"], c ="black", s=1, label= "Random")

plt.rc('axes', axisbelow=True)
plt.xlabel("Update Times")
plt.ylabel("Reward")

# plt.ylim([100,350])
plt.grid()
plt.legend()
plt.show()