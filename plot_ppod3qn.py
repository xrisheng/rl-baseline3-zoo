import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

import numpy as np
import pandas as pd


import matplotlib.pyplot as plt
import csv
  

df1 = pd.read_csv("logs/run-PPO_1-tag-eval_mean_reward.csv")
df2 = pd.read_csv("logs/run-D3QN_1-tag-eval_mean_reward.csv")

x1 = df1["Step"].to_list()
y1 = df1["PPO"].to_list()
x2 = df2["Step"].to_list()
y2 = df2["D3QN"].to_list()

plt.plot(x1,y1, label='PPO')
plt.plot(x2,y2, label='D3QN')
plt.rc('axes', axisbelow=True)
plt.xlabel("Train Step")
plt.ylabel("Reward")

# plt.ylim([100,350])
plt.grid()
plt.legend()
plt.show()