import json
import random

src_path = "data/mb.json"
dst_path = "data/mb4.json"
json_file = open(src_path)
data = json.load(json_file)

for rnb in data["Runnables"]:
    len = rnb["Length"]
    v = 1 if random.random() < 0.5 else -0.5

    rnb["Length"] = len * (1 + v)

with open(dst_path, "w+") as f:
    json.dump(data, f, indent=2)

