import time
import random
import sys
import pandas as pd
from deap import creator, base, tools, algorithms
from scheduling.scheduling_analyzer import SchedulingAnalyzer
from scheduling.scheduling_eval import *


dic = {
    "npop": 200,
    "ngen": 120,
    "mut_k": 50,
    "mat_k": 220,
    "tournsize": 3,
    "cxpb": 0.5,
    "mutpb": 0.1,
}


class Container(object):
    # This class does not require the fitness attribute
    # it will be  added later by the creator
    def __init__(self, a: SchedulingAnalyzer):
        # Some initialisation with received values
        self.analyzer = a.copy_const()


def init_individual(ind_class, a):
    # ind_class will receive a class inheriting from Container
    ind = ind_class(a)
    ind.analyzer.reset()
    ind.analyzer.rnd_scheduling()
    return ind


def mutation(ind, mut_k=200):
    mut_rnb_idxs = random.sample(range(len(ind.analyzer.runnables)), mut_k)
    mut_rnb_idxs = set(mut_rnb_idxs)
    old_alloc = {rnb.idx: rnb.frame.idx for rnb in ind.analyzer.runnables}
    ind.analyzer.reset()

    for rnb in ind.analyzer.runnables:
        avbl_frame_idxs = [frame.idx for frame in rnb.avbl_frames if frame.idx != old_alloc[rnb.idx]]
        random.shuffle(avbl_frame_idxs)

        if rnb.idx not in mut_rnb_idxs:
            avbl_frame_idxs.insert(0, old_alloc[rnb.idx])
        else:
            avbl_frame_idxs.append(old_alloc[rnb.idx])

        for idx in avbl_frame_idxs:
            if not ind.analyzer.frames[idx].is_full(rnb.length):
                _, _ = ind.analyzer.update(rnb, ind.analyzer.frames[idx])
                break

        if rnb.frame is None:
            print("not mutable runnable " + rnb.idx)
            ind.analyzer.acc_rew_total -= 10000

    return (ind,)


def cx_analyzer(left, right, mat_k=200):
    cx_rnb_idxs = random.sample(range(len(left.analyzer.runnables)), mat_k)
    cx_rnb_idxs = set(cx_rnb_idxs)
    left_alloc = {rnb.idx: rnb.frame.idx for rnb in left.analyzer.runnables}
    right_alloc = {rnb.idx: rnb.frame.idx for rnb in right.analyzer.runnables}
    left.analyzer.reset()
    right.analyzer.reset()

    for rnb in left.analyzer.runnables:
        avbl_frame_idxs = [frame.idx for frame in rnb.avbl_frames
                           if frame.idx != right_alloc[rnb.idx] and frame.idx != left_alloc[rnb.idx]]

        random.shuffle(avbl_frame_idxs)

        if rnb.idx not in cx_rnb_idxs:
            avbl_frame_idxs.insert(0, left_alloc[rnb.idx])
            avbl_frame_idxs.append(right_alloc[rnb.idx])
        else:
            avbl_frame_idxs.insert(0, right_alloc[rnb.idx])
            avbl_frame_idxs.append(left_alloc[rnb.idx])

        for idx in avbl_frame_idxs:
            if not left.analyzer.frames[idx].is_full(rnb.length):
                _, _ = left.analyzer.update(rnb, left.analyzer.frames[idx])
                break

        if rnb.frame is None:
            print("not mateble runnable " + rnb.idx)
            left.analyzer.acc_rew_total -= 10000

    for rnb in right.analyzer.runnables:
        avbl_frame_idxs = [frame.idx for frame in rnb.avbl_frames
                           if frame.idx != right_alloc[rnb.idx] and frame.idx != left_alloc[rnb.idx]]

        random.shuffle(avbl_frame_idxs)

        if rnb.idx not in cx_rnb_idxs:
            avbl_frame_idxs.insert(0, right_alloc[rnb.idx])
            avbl_frame_idxs.append(left_alloc[rnb.idx])
        else:
            avbl_frame_idxs.insert(0, left_alloc[rnb.idx])
            avbl_frame_idxs.append(right_alloc[rnb.idx])

        for idx in avbl_frame_idxs:
            if not right.analyzer.frames[idx].is_full(rnb.length):
                _, _ = right.analyzer.update(rnb, right.analyzer.frames[idx])
                break

        if rnb.frame is None:
            print("not mateble runnable " + rnb.idx)
            right.analyzer.acc_rew_total -= 10000

    return left, right


def evaluate(ind):
    return (ind.analyzer.acc_rew_total,)


if __name__ == '__main__':
    sys.setrecursionlimit(100000)

    data_path = "data/mb4.json"
    sp = data_path.split("/")
    sp = sp[1]
    sp = sp.split(".")
    sp = sp[0]
    sa = SchedulingAnalyzer()
    sa.load_data(data_path)

    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", Container, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()
    toolbox.register("individual", init_individual, creator.Individual, a=sa)
    toolbox.register("evaluate", evaluate)
    toolbox.register("mutate", mutation, mut_k=dic["mut_k"])
    toolbox.register("mate", cx_analyzer, mat_k=dic["mat_k"])
    toolbox.register("select", tools.selTournament, tournsize=dic["tournsize"])
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    pop = toolbox.population(n=dic["npop"])
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    # pool = multiprocessing.Pool(processes=32)
    # toolbox.register("map", pool.map)
    start_time = time.time()
    _, logbook = algorithms.eaSimple(pop, toolbox, cxpb=dic["cxpb"], mutpb=dic["mutpb"], ngen=dic["ngen"],
                        stats=stats, halloffame=hof)

    exec_time = time.time() - start_time
    best_model = hof.items[0].analyzer

    res = []
    for rnb in best_model.runnables:
        res.append(rnb.frame.idx)

    dic["exec_time"] = time.time() - start_time
    dic["reward"] = best_model.acc_rew_total
    dic["result"] = res

    df_log = pd.DataFrame(logbook)

    timestr = time.strftime('%Y%m%d-%H%M%S')
    logstr = 'logs/ga/log_' + sp +'_'+ timestr + '.csv'
    modelstr = 'logs/ga/model_' + sp +'_' + timestr + '.json'

    df_log.to_csv(logstr, index=False)

    with open(modelstr, 'w') as f:
        json.dump(dic, f)

   # pool.close()
    pass
