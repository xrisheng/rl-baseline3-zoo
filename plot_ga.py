import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

import numpy as np
import pandas as pd


import matplotlib.pyplot as plt
import csv
  

df1 = pd.read_csv("logs/ga/log_20221021-162909.csv")

x1 = df1["gen"].to_list()
y1 = df1["max"].to_list()

plt.plot(x1,y1, label='GA')
plt.rc('axes', axisbelow=True)
plt.xlabel("Generation")
plt.ylabel("Reward")

# plt.ylim([100,350])
plt.grid()
plt.legend()
plt.show()